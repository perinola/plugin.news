<?php
/**
 * 
 */
class Noticias extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('plugins/news_model', 'news_model');
		
		$this->output->enable_profiler(FALSE);
	}
	public function index(){		
		$dates 			= $this->news_model->date_navigation();
		$years_array 	= array_keys($dates);
		$year 			= array_pop($years_array);
		$month 			= array_pop($dates[$year]);
		
		$this->desplegar($year, $month);
	}
	public function desplegar($year, $month, $id= NULL){
		@$data['date_navigation']		= $this->news_model->date_navigation();
		@$data['news']					= $this->news_model->news_list($year, $month);
		@$data['current']				= $this->news_model->current_news($year, $month, $id);
		
		$this->load->template('noticias', $data);
	}
}
