<?php
/**
 * @author 	Guido A. Orellana
 * @name	Plugin Noticias
 * @since	septiembre 2013
 * 
 */
class Plugin_noticias extends PL_Controller {
	
	function __construct(){
		parent::__construct();
		
		//Load the plugin data
		$this->plugin_action_table			= 'PLUGIN_NEWS';
		$this->plugin_button_create			= "Crear Nueva Noticia";
		$this->plugin_button_cancel			= "Cancelar";
		$this->plugin_button_update			= "Guardar Cambios";
		$this->plugin_button_delete			= "Eliminar";
		$this->plugin_page_title			= "Noticias";
		$this->plugin_page_create			= "Crear Nueva Noticia";
		$this->plugin_page_read				= "Mostrar Noticia";
		$this->plugin_page_update			= "Editar Noticia";
		$this->plugin_page_delete			= "Eliminar";
		$this->plugin_display_array[0]		= "ID";
		$this->plugin_display_array[1]		= "T&iacute;tulo";
		$this->plugin_display_array[2]		= "Imagen";
		$this->plugin_display_array[3]		= "Contenido";
		
		$this->plugins_model->initialise($this->plugin_action_table);
		
		//Extras to send
		$this->plugin_image_route			= "/user_files/uploads/images/";
		$this->image_max_width				= 300; //M�ximo ancho de la imagen
		$this->image_max_height				= 300; //M�ximo alto de la imagen
		$this->file_input					= "NEWS_IMAGE";
		
		$this->display_pagination			= TRUE; //Mostrar paginaci�n en listado
		$this->pagination_per_page			= 10; //Numero de registros por p�gina
		$this->pagination_total_rows		= $this->plugins_model->total_rows(); //N�mero total de items a desplegar
		
		$this->display_filter				= FALSE; //Mostrar filtro de b�squeda 'SEARCH' o seg�n listado 'LIST' o no mostrar FALSE
	}
	
	/**
	 * Funci�n para desplegar listado completo de datos guardados, enviar los t�tulos en array con clave header y el cuerpo en un array con clave body.
	 * Para editar fila es a la funci�n 'update_table_row'
	 * 
	 * @param	$result_array 		array 		Array con la listado devuelto por query de la DB
	 * @return	$data_array 		array 		Arreglo con la informaci�n del [header] y [body]
	 * 											Env�a el key [filteroptions] con las opciones de un filtro en listado y [currentFilter] con el resultado actual.
	 */
	public function _html_plugin_display($result_array){
		
		//Header data
		$data_array['header'][1]			= $this->plugin_display_array[1];
		$data_array['header'][2]			= $this->plugin_display_array[3];
		
		//Body data
		$data_array['body'] = '';
		foreach($result_array['body'] as $field):
		$data_array['body']					.= '<tr>';
		$data_array['body']					.= '<td><a href="'.base_url('cms/'.strtolower($this->current_plugin).'/update_table_row/'.$field->ID).'">'.$field->NEWS_TITLE.'</a></td>';
		$data_array['body']					.= '<td>'.character_limiter($field->NEWS_CONTENT, 90).'</td>';
		$data_array['body']					.= '</tr>';
		endforeach;
		
		//
		
		return $data_array;
	}
	
	/*
	 * Funci�n para crear nuevo contenido, desde aqu� se especifican los campos a enviar en el formulario.
	 * El formulario se env�a mediante objectos preestablecidos de codeigniter. 
	 * El formulario se env�a con un array con la clave form_html.
	 * Se puede encontrar una gu�a en: http://ellislab.com/codeigniter/user-guide/helpers/form_helper.html
	 */
	public function _html_plugin_create(){
        
		//Formulario
		$data_array['form_html']			 = "<div class='control-group'>".form_label($this->plugin_display_array[1],'',array('class' => 'control-label'))."<div class='controls'>".form_input(array('name' => 'NEWS_TITLE', 'class' => 'span6'))."</div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[2],'',array('class' => 'control-label'))."<div class='controls'>".form_upload(array('name' => 'NEWS_IMAGE', 'class' => 'span6'))."<span class='help-block'>".$this->image_max_width." x ".$this->image_max_height."px</span></div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[3],'',array('class' => 'control-label'))."<div class='controls'>".form_textarea(array('name' => 'NEWS_CONTENT', 'class' => 'span6 textarea'))."<span class='help-block'>*No pegar texto directamente desde word</span></div></div>";
		return $data_array;
    }
	public function _html_plugin_update($result_data){
		
		//Formulario
		$data_array['form_html']			 = "<div class='control-group'>".form_label($this->plugin_display_array[1],'',array('class' => 'control-label'))."<div class='controls'>".form_input(array('name' => 'NEWS_TITLE','value' => $result_data->NEWS_TITLE, 'class' => 'span6'))."</div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[2],'',array('class' => 'control-label'))."<div class='controls'>".form_upload(array('name' => 'NEWS_IMAGE', 'class' => 'span6'))."<span class='help-block'>".$this->image_max_width." x ".$this->image_max_height."px</span></div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[3],'',array('class' => 'control-label'))."<div class='controls'>".form_textarea(array('name' => 'NEWS_CONTENT','value' => $result_data->NEWS_CONTENT, 'class' => 'span6 textarea'))."<span class='help-block'>*No pegar texto directamente desde word</span></div></div>";
		
		return $data_array;
	}
	
	
	/**
	 * Funciones para editar Querys o Datos a enviar desde cada plugin
	 */
	//Funci�n para desplegar listado, desde aqu� se puede modificar el query
	public function _plugin_display($filter){
		$offset = (isset($filter[2]))?$filter[2]:0;
		$result_array['body'] = $this->plugins_model->list_rows('', '', $this->pagination_per_page, $offset, 'NEWS_DATE DESC');
		
		
		return $this->_html_plugin_display($result_array);
	}
	//Funciones de los posts a enviar
	public function post_new_val(){
		$submit_posts 				= $this->input->post();
		$addimg = $this->upload_image();
		$submit_posts['NEWS_IMAGE']	= ($addimg)?$addimg['file_name']:NULL;
		$submit_posts['NEWS_DATE']	= date('Y-m-d');
		
		return $this->_set_new_val($submit_posts);
	}
	public function post_update_val($data_id){
		$submit_posts 				= $this->input->post();
		$submit_posts['ID']			= $data_id;
		$addimg = $this->upload_image();
		if($addimg)
		$submit_posts['NEWS_IMAGE']	= $addimg['file_name'];
		
		$submit_posts['NEWS_DATE']	= date('Y-m-d');
		
		return $this->_set_update_val($submit_posts);
	}
	
	/**
	 * Funciones espec�ficas del plugin
	 */
	 private function upload_image(){
	 	
			//Si se carga una imagen
			if(!empty($_FILES[$this->file_input]["name"])):
				$upload_config['upload_path'] 		= '.'.$this->plugin_image_route;
				$upload_config['overwrite']			= TRUE;
				$upload_config['allowed_types'] 	= 'gif|jpg|png';
				$upload_config['max_width']  		= $this->image_max_width;
				$upload_config['max_height'] 		= $this->image_max_height;
				$this->upload->initialize($upload_config);
				
				if (!$this->upload->do_upload($this->file_input)):
					return false;
					$this->fw_alerts->add_new_alert(4002, 'ERROR');
				else:
					return $uploaded_data = $this->upload->data();
					$this->fw_alerts->add_new_alert(4001, 'SUCCESS');
				endif;
			endif;
	 }
}