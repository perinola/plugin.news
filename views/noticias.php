<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    // init the FB JS SDK
    FB.init({
      appId      : '180218785344674',                        // App ID from the app dashboard
      channelUrl : 'http://empleoclick.com/facebook_api', // Channel file for x-domain comms
      status     : true,                                 // Check Facebook Login status
      xfbml      : true                                  // Look for social plugins on the page
    });

    // Additional initialization code such as adding Event Listeners goes here
  };

  // Load the SDK asynchronously
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/all.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

   <div id="content"> <!-- content -->
     <div id="inner-content"> <!-- inner-content -->       
	  <div class="mainpane"><!-- mainpane -->
	   <div class="person-inner"><!-- person-inner -->
	     <div class="news"><!-- news -->
	       <div class="tab-top"><h5>Actualidad</h5> </div>
		     <div class="post"><!-- post -->
				 <h3><?php echo $current->NEWS_TITLE?></h3>
				 <div class="meta">Publicado el <?php echo mysql_date_to_dmy($current->NEWS_DATE)?></div>	 
				 
				 <img src="<?php echo base_url('user_files/uploads/images/'.$current->NEWS_IMAGE)?>" alt="post img">
				 <p><?php echo $current->NEWS_CONTENT?></p>
				 
				 <!-- COMENTARIOS FACEBOOK -->
				 <ul class="like">
					 <li><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.empleoclick.com%2F&amp;width=90&amp;height=21&amp;colorscheme=light&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;send=false&amp;appId=162431488586" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:90px; height:21px;" allowTransparency="true"></iframe></li>
					 <li><iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://platform.twitter.com/widgets/tweet_button.html" style="width:80px; height:20px;"></iframe></li>
					 <li class="list3"><a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fempleoclick.com%2F&media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg&description=Next%20stop%3A%20Pinterest" data-pin-do="buttonPin" data-pin-config="beside"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a></li>
					 <li><script src="//platform.linkedin.com/in.js" type="text/javascript">lang: es_ES </script><script type="IN/Share" data-url="http://empleoclick.com/" data-counter="right"></script></li>
				  </ul>
				<div class="respond">
				<div class="res-top"><h6>Comentarios</h6></div>
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=222666624430491";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				
				<div class="fb-comments" data-href="<?php echo current_url()?>" data-width="560"></div>
				</div><!-- respond -->
			</div><!-- / post -->
			
			<div class="sidebar"><!-- sidebar -->			
			 <div class="widget">
			   <h6>RECIENTES</h6>
			  <ul class="recent">
			  	<?php foreach($news as $news):?>
				<li>
					<a href="<?php echo site_url('noticias/desplegar/'.$news->NEWS_YEAR.'/'.$news->NEWS_MONTH.'/'.$news->ID.'/'.convert_accented_characters(str_replace(' ', '_', $news->NEWS_TITLE)))?>">
						<span class="image_news"><img src="<?php echo base_url('user_files/uploads/images/'.$news->NEWS_IMAGE)?>" width="55"> </span>
						<strong><?php echo mysql_date_to_dmy($news->NEWS_DATE)?> - <?php echo word_limiter($news->NEWS_TITLE, 8)?></strong>
				 		<span><?php echo character_limiter($news->NEWS_CONTENT, 90)?></span>
				 	</a>
				 </li>
				 <?php endforeach?>
			  </ul>
			</div>
			
			
			<div class="widget">
				<h6>ARCHIVO</h6>
				<ul class="archivo">
					<?php foreach($date_navigation as $year => $months):?>
					<?php foreach($months as $month):
					$datehelper = date_components('sp');?>
					<li><a href="<?php echo site_url('noticias/desplegar/'.$year.'/'.$month)?>"><?php echo strtoupper($datehelper['meses'][$month]).' '.$year;?></a></li>
					<?php endforeach?>
					<?php endforeach?>
				</ul>
			</div>
			
		  </div><!-- / sidebar -->	
		 </div><!-- / news -->
	   </div> <!-- / person-inner -->	 
	  </div><!-- / mainpane -->	 
	</div><!-- / inner-content -->
</div> <!-- / content -->