<?php
/**
 * Obtener las noticias recientes
 */
class News_model extends MY_Model {
	
	function __construct() {
		parent::__construct();
		$this->set_table('PLUGIN_NEWS');
	}
	
	/**
	 * Obtener las fechas de las noticias agregadas separadas por meses del a�o
	 */
	public function date_navigation(){
		$this->db->select("YEAR(NEWS_DATE) AS NEWS_YEAR, DATE_FORMAT(NEWS_DATE, '%m') AS NEWS_MONTH", FALSE);
		$this->db->from('PLUGIN_NEWS');
		$this->db->group_by(array('MONTH(NEWS_DATE)', 'YEAR(NEWS_DATE)'));
		$this->db->order_by('YEAR(NEWS_DATE) DESC, MONTH(NEWS_DATE) DESC');
		$query = $this->db->get();
		
		$results = $query->result();
		
		foreach($results as $result)
		$newsdate[$result->NEWS_YEAR][] = $result->NEWS_MONTH;
		
		return $newsdate;
	}
	/**
	 * Obtener el listado de noticias
	 */
	 public function news_query($year, $month){
		$query = $this->db->select('*, YEAR(NEWS_DATE) AS NEWS_YEAR, MONTH(NEWS_DATE) AS NEWS_MONTH')
		->from('PLUGIN_NEWS')
		->where('YEAR(NEWS_DATE)', $year)
		->where('MONTH(NEWS_DATE)', $month);
		
		return $query;
	 }
	/**
	 * Obtener listado de noticias
	 */
	 public function news_list($year, $month){
		$query = $this->news_query($year, $month)->order_by('NEWS_DATE DESC')->get();
		
		return $query->result();
	 }
	 /**
	  * Obtener la fecha espec�fica
	  */
	 public function current_news($year, $month, $id = NULL){
		$query = $this->news_query($year, $month);
		$query = (!empty($id))?$query->where('ID', $id):$query->order_by('NEWS_DATE DESC');
		
		$query = $query->limit(1)->get();
		
		return $query->row();
	 }
}
